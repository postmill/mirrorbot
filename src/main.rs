extern crate archiveis;
extern crate futures;
extern crate tokio_core;

use std::fs;
use std::mem;
use std::str::FromStr;
use chrono::prelude::*;
use clap::{Arg, App};
use postmill::*;

mod archive;
mod tor;

fn string_to_static_str(s: String) -> &'static str {
    unsafe {
        let ret = mem::transmute(&s as &str);
        mem::forget(s);
        ret
    }
}

fn main() {
    let matches = App::new("mirrorbot")
        .version("0.1")
        .about("Raddle.me url mirrors")
        .arg(Arg::with_name("forum_url")
             .short("f")
             .long("forum_url")
             .value_name("URL")
             .takes_value(true))
        .get_matches();

    // Try to see if there is a post earlier than today
    let last_time = match fs::read_to_string("last_time.txt") {
        Ok(last) => DateTime::parse_from_rfc3339(&last.lines().next().unwrap()).unwrap(),
        Err(_) => {
            fs::write("last_time.txt", Utc::now().to_rfc3339()).unwrap();
            println!("No previous time found, writing new time!");
            return;
        }
    };

    let mut client = Client::new("https://raddle.me").unwrap();

    // Login
    client.login("MirrorBot", include_str!("../password.txt").trim()).unwrap();

    let img_urls = vec!["i.imgur.com", "i.redd.it"];

    // Get all the submissions from the supplied page
    client.submissions_from_page_until(matches.value_of("forum_url").unwrap_or("all/new"), last_time).unwrap()
        // Turn the vector into an iterator
        .iter()
        .rev()
        .for_each(|sub| {
            let mut comment = String::new();

            // Tor bot
            if let Some(torified) = tor::torify_url(sub.url.to_string()) {
                comment = format!("{}\n{}", comment, &*torified);
            }

            // Archive bot
            if let Some(archived) = archive::archive_url(sub.url.to_string()) {
                comment = format!("{}\n{}", comment, &*archived);
            }

            // Shred bot
            if img_urls.contains(&sub.url.domain().unwrap()) {
                // Download the image from ImGur
                let mut resp = reqwest::get(sub.url.clone()).unwrap();
                let mut buf: Vec<u8> = vec![];
                resp.copy_to(&mut buf).unwrap();

                let resp_type = mime::Mime::from_str(resp.headers().get(reqwest::header::CONTENT_TYPE).unwrap().to_str().unwrap()).unwrap();

                let form = reqwest::multipart::Form::new()
                    .text("desired_filename", string_to_static_str(sub.url.path().trim_start_matches('/').to_string()))
                    .part("uploadcoin", reqwest::multipart::Part::bytes(buf).file_name("file").mime_str(resp_type.as_ref()).unwrap());

                let req = reqwest::Client::new();
                // Upload the file to coinsh.red
                let resp = req.post("https://coinsh.red/private/beam.php")
                    .multipart(form)
                    .send().unwrap();

                // Check the status
                let query = resp.url().query();
                let id_or_not = match query {
                    Some("error=1") => {
                        println!("\tError: nothing was uploaded");
                        None
                    },
                    Some("error=2") => {
                        println!("\tError: file already exists on server");

                        let id = sub.url.path().trim_start_matches('/').to_string();
                        Some(id)
                    },
                    Some(_) => {
                        let status = resp.url().query_pairs().next().unwrap();
                        assert_eq!(status.0, "success");
                        Some(status.1.to_string())
                    },
                    None => {
                        println!("\tReceived weird URL back: \"{}\"", resp.url());
                        None
                    }
                };

                if let Some(id) = id_or_not {
                    let url = format!("https://coinsh.red/p/{}", id);
                    let tor_url = format!("http://qt5gjwdgdwpkt5x5.onion/p/{}", id);

                    // Attempt to change the url
                    if client.edit_post(sub.id, Some(&*url), None, None).is_err() {
                        comment = format!("{}\n[Coinsh.red mirror]({})  ", comment, url);
                    }
                    comment = format!("{}\n[Coinsh.red Tor mirror]({})  ", comment, tor_url);
                }
            }

            // Post the comment
            if !comment.is_empty() {
                comment = format!("{}\n###### _[PM](/compose_message/Fossidarity) me if the URLs are broken._", comment);

                client.comment_post(sub.id, &*comment).unwrap();

                fs::write("last_time.txt", sub.date.to_rfc3339()).expect("Could not write time to file");
            }
        });

    fs::write("last_time.txt", Utc::now().to_rfc3339()).unwrap();
}
