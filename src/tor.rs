use std::collections::HashMap;
use url::Url;

pub fn torify_url<'a>(mut clearnet: String) -> Option<String> {
    // Remove the www part
    clearnet = clearnet.replace("www.", "");

    // It's a Raddle URL, these are automatically converted to tor links
    if clearnet.chars().next() == Some('/') {
        return None;
    }

    let mut url = Url::parse(clearnet.as_str()).unwrap();
    match get_url_maps().get(url.host_str().unwrap()) {
        Some((onion, source)) => {
            url.set_host(Some(onion)).unwrap();
            url.set_scheme("http").unwrap();

            println!("{}", clearnet);

            Some(format!("[Tor URL]({}), [verification]({})  ", url.as_str(), source))
        }
        None => None
    }
}

fn get_url_maps() -> HashMap<&'static str, (&'static str, &'static str)> {
    let mut map = HashMap::new();

    // Raddle
    //map.insert("raddle.me", ("lfbg75wjgi4nzdio.onion", "https://raddle.me/wiki/faq"));

    // Protonmail
    map.insert("protonmail.com", ("protonirockerxow.onion", "https://github.com/alecmuffett/real-world-onion-sites"));
    // DuckDuckGo
    map.insert("duckduckgo.com", ("3g2upl4pq6kufc4m.onion", "https://github.com/alecmuffett/real-world-onion-sites"));
    // Mail2Tor
    map.insert("mail2tor.com", ("mail2tor2zyjdctd.onion", "https://github.com/alecmuffett/real-world-onion-sites"));
    // Facebook
    map.insert("facebook.com", ("facebookcorewwwi.onion", "https://github.com/alecmuffett/real-world-onion-sites"));
    // Blockchain.Info
    map.insert("blockchain.info", ("blockchainbdgpzk.onion", "https://github.com/alecmuffett/real-world-onion-sites"));
    // Riseup
    map.insert("riseup.net", ("xpgylzydxykgdqyg.onion", "https://github.com/alecmuffett/real-world-onion-sites"));
    // The New York Times
    map.insert("nytimes.com", ("nytimes3xbfgragh.onion", "https://github.com/alecmuffett/real-world-onion-sites"));
    // BuzzFeed News
    map.insert("buzzfeednews.com", ("bfnews3u2ox4m4ty.onion", "https://github.com/alecmuffett/real-world-onion-sites"));
    // ProPublica
    map.insert("propublica.org", ("propub3r6espa33w.onion", "https://github.com/alecmuffett/real-world-onion-sites"));
    // Debian
    map.insert("debian.org", ("sejnfjrq6szgca7v.onion", "https://github.com/alecmuffett/real-world-onion-sites"));
    // ExpressVPN
    map.insert("expressvpn.com", ("expressobutiolem.onion", "https://github.com/alecmuffett/real-world-onion-sites"));
    // Cyph
    map.insert("cyph.com", ("cyphdbyhiddenbhs.onion", "https://github.com/alecmuffett/real-world-onion-sites"));
    // Tor Project
    map.insert("torproject.org", ("expyuzz4wqqyqhjn.onion", "https://github.com/alecmuffett/real-world-onion-sites"));
    // Hardened BSD
    map.insert("hardenedbsd.org", ("dxsj6ifxytlgq33k.onion", "https://github.com/alecmuffett/real-world-onion-sites"));
    // Whonix
    map.insert("whonix.org", ("kkkkkkkkkk63ava6.onion", "https://github.com/alecmuffett/real-world-onion-sites"));
    // Qubes OS
    map.insert("qubes-os.org", ("sik5nlgfc5qylnnsr57qrbm64zbdx6t4lreyhpon3ychmxmiem7tioad.onion", "https://github.com/alecmuffett/real-world-onion-sites"));
    // keybase.io
    map.insert("keybase.io", ("fncuwbiisyh6ak3i.onion", "https://github.com/alecmuffett/real-world-onion-sites"));
    // Mailpile
    map.insert("mailpile.is", ("clgs64523yi2bkhz.onion", "https://github.com/alecmuffett/real-world-onion-sites"));

    // Archive.is
    map.insert("archive.is", ("archivecaslytosk.onion", "https://en.wikipedia.org/wiki/Archive.is"));
    map.insert("archive.today", ("archivecaslytosk.onion", "https://en.wikipedia.org/wiki/Archive.is"));
    map.insert("archive.li", ("archivecaslytosk.onion", "https://en.wikipedia.org/wiki/Archive.is"));
    map.insert("archive.fo", ("archivecaslytosk.onion", "https://en.wikipedia.org/wiki/Archive.is"));

    // gitlab.com
    map.insert("gitlab.com", ("haklab4ulibiyeix.onion", "http://hiddenwikitor.org/"));

    // ProPublica
    map.insert("propublica.org", ("propub3r6espa33w.onion", "https://www.propublica.org/nerds/a-more-secure-and-anonymous-propublica-using-tor-hidden-services"));

    // Write.as
    map.insert("write.as", ("writeas7pm7rcdqg.onion", "https://www.reddit.com/r/onions/comments/77lxey/simple_tor_blogs_writeas/"));

    // YouTube
    map.insert("youtube.com", ("kgg2m7yk5aybusll.onion", "https://www.reddit.com/r/TOR/comments/a3c1ak/you_can_now_watch_youtube_videos_anonymously_with/"));
    map.insert("youtu.be", ("kgg2m7yk5aybusll.onion", "https://www.reddit.com/r/TOR/comments/a3c1ak/you_can_now_watch_youtube_videos_anonymously_with/"));
    map.insert("invidio.us", ("kgg2m7yk5aybusll.onion", "https://www.reddit.com/r/TOR/comments/a3c1ak/you_can_now_watch_youtube_videos_anonymously_with/"));

    // Coinsh.red
    map.insert("coinsh.red", ("4wxnumwn5zljjkyi.onion", "/u/jadedctrl"));

    map
}
