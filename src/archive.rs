use url::Url;
use archiveis::ArchiveClient;
use futures::future::Future;
use tokio_core::reactor::Core;

pub fn archive_url<'a>(mut source_url: String) -> Option<String> {
    // Remove the www part
    source_url = source_url.replace("www.", "");

    // It's a Raddle URL, these are automatically converted to tor links
    if source_url.chars().next() == Some('/') {
        return None;
    }

    let url = Url::parse(source_url.as_str()).unwrap();
    let host_str = url.host_str().unwrap();
    if host_str == "raddle.me" || host_str == "lfbg75wjgi4nzdio.onion" {
        return None;
    }

    let mut result = None;
    let mut core = Core::new().unwrap();
    let client = ArchiveClient::new(Some("archivebot"));
    let capture = client.capture(url.as_str())
        .and_then(|archived| {
            result = Some(archived.archived_url);
            Ok(())
        });
    core.run(capture).unwrap_or(());

    match result {
        None => None,
        Some(url) => {
            let mut tor_url = Url::parse(&url).unwrap();
            tor_url.set_host(Some("archivecaslytosk.onion")).unwrap();
            Some(format!("[Archive URL]({})  \n[Archive Tor URL]({})  ", url.as_str(), tor_url.as_str()))
        }
    }
}
